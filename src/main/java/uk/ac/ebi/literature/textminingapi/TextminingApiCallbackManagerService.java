package uk.ac.ebi.literature.textminingapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import uk.ac.ebi.literature.textminingapi.pojo.Components;
import uk.ac.ebi.literature.textminingapi.pojo.Failure;
import uk.ac.ebi.literature.textminingapi.pojo.FileInfo;
import uk.ac.ebi.literature.textminingapi.pojo.Status;
import uk.ac.ebi.literature.textminingapi.pojo.SubmissionMessage;
import uk.ac.ebi.literature.textminingapi.service.MLQueueSenderService;
import uk.ac.ebi.literature.textminingapi.service.MongoService;
import uk.ac.ebi.literature.textminingapi.utility.Utility;

@Service
public class TextminingApiCallbackManagerService {

	private static final Logger log = LoggerFactory.getLogger(TextminingApiCallbackManagerApplication.class);

	private final MLQueueSenderService mlQueueSenderService;

	private final MongoService mongoService;

	private final RestTemplate restTemplate;

	private final RetryTemplate retryTemplate;

	public TextminingApiCallbackManagerService(MLQueueSenderService mlQueueSenderService, MongoService mongoService, RestTemplate restTemplate, RetryTemplate retryTemplate) {
		this.mlQueueSenderService = mlQueueSenderService;
		this.mongoService = mongoService;
		this.restTemplate = restTemplate;
		this.retryTemplate = retryTemplate;
	}

	@RabbitListener(autoStartup = "${rabbitmq.callbackQueue.autoStartUp}", queues = "${rabbitmq.callbackQueue}")
	public void listenForMessage(Message message) throws Exception {
		var submissionMessage = Utility.castMessage(message, SubmissionMessage.class);

		log.info("{" + submissionMessage.getFtId() + ", " + submissionMessage.getUser()
				+ "} Message arrived with Status {" + submissionMessage.getStatus() + "}");

		log.info("Submission Message " + submissionMessage);

		if (mlQueueSenderService.hasExceededRetryCount(message)) {
			log.info("{" + submissionMessage.getFtId() + ", " + submissionMessage.getUser() + "} retry count exceeded");
			var failure = Failure.valueOf(submissionMessage.getFtId(), submissionMessage.getUser(), "",
					Components.CALLBACKMANAGER.getLabel(), submissionMessage);
			this.mongoService.storeFailure(failure);
			return;
		}

		if (submissionMessage.getStatus().equals(Status.PENDING.getLabel()))
			throw new RuntimeException("SubmissingMessage is Invalid");

		var requestBody = transformSubmissionMessage(submissionMessage);

		log.info("Transoformed Request ->" + requestBody);

		String url = submissionMessage.getCallback();

		log.info("{" + submissionMessage.getFtId() + ", " + submissionMessage.getUser()
				+ "} submitting request to callback {" + url + "}");

		ResponseEntity<String> response = retryTemplate.execute((context) -> {
			log.info("{" + submissionMessage.getFtId() + ", " + submissionMessage.getUser() + "} retrying...{" + url
					+ "}");
			return this.restTemplate.postForEntity(url, requestBody, String.class);
		});

		if (response.getStatusCode() != HttpStatus.OK)
			throw new RuntimeException("Error while submitting request to callback {" + url + "}");

		log.info("{" + submissionMessage.getFtId() + ", " + submissionMessage.getUser()
				+ "} Request to callback was successfull!!");
	}


	public final SubmissionMessage transformSubmissionMessage(SubmissionMessage submissionMessage) throws Exception {
		if (submissionMessage.getStatus().equals(Status.SUCCESS.getLabel()))
			return transformToSuccessfulMessage(submissionMessage);
		else
			return transformToFailedMessage(submissionMessage);
	}

	private final SubmissionMessage transformToSuccessfulMessage(SubmissionMessage submissionMessage) {
		var success = new SubmissionMessage();
		success.setFtId(submissionMessage.getFtId());
		success.setStatus(Status.SUCCESS.getLabel());
		success.setFiles(submissionMessage.getFiles());
		return success;
	}

	private final SubmissionMessage transformToFailedMessage(SubmissionMessage submissionMessage) {
		var failed = new SubmissionMessage();
		failed.setFtId(submissionMessage.getFtId());
		failed.setStatus(Status.FAILED.getLabel());
		FileInfo[] files = new FileInfo[submissionMessage.getFiles().length];
		for (int i = 0; i < submissionMessage.getFiles().length; i++) {
			var file = submissionMessage.getFiles()[i];
			var fileInfo = new FileInfo();
			if (file.getStatus().equals(Status.FAILED.getLabel())) {
				fileInfo.setError(file.getError());
				fileInfo.setErrorComponent(file.getErrorComponent());
			} else {
				fileInfo.setUrlFetch(file.getUrlFetch());
			}
			fileInfo.setStatus(file.getStatus());
			fileInfo.setFilename(file.getFilename());
			fileInfo.setUrl(file.getUrl());
			files[i] = fileInfo;
		}
		failed.setFiles(files);
		return failed;
	}
}
