package uk.ac.ebi.literature.textminingapi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@PropertySources({ @PropertySource("classpath:application-utility.properties"),
		@PropertySource("classpath:application-utility-${spring.profiles.active}.properties"), })
@SpringBootApplication
public class TextminingApiCallbackManagerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(TextminingApiCallbackManagerApplication.class);
		app.run(args);
	}

	public void run(String... args) throws Exception {

	}

}
